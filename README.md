Demo of an issue where the "cherry-pick" button only shows up
for merge requests completed under a "Merge Commit" setting
but if the repo is set to "Fast-forward Merges"
The "cherry-pick" button doesn't show up on the merged MR screen

# This merge request

Should result in a merge commit
The repo setting when I merge this was "Merge Commit"

# The FF merge request

This should result in a fast-forward merge
I would like to be offered the option to cherry-pick after this commit goes in
